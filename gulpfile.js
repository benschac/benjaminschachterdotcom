var gulp = require('gulp'),
	sass = require('gulp-sass'),
	browserSync = require('browser-sync'),
	useref = require('gulp-useref'),
	uglify = require('gulp-uglify'),
	gulpIf = require('gulp-if'),
	minifyCSS = require('gulp-minify-css'),
	imagemin = require('gulp-imagemin'),
	cache = require('gulp-cache'),
	del = require('del'),
	runSequence = require('run-sequence'),
	notify = require('gulp-notify'),
	bower = require('gulp-bower');



// configuration object


gulp.task('bower', function() {
	return bower('./bower_components').pipe(gulp.dest('app/lib'));
});


// start build tasks
gulp.task('task-name', function(cb) {
	runSequence('clean:dist', ['sass', 'useref', 'images', 'fonts'], cb )
});

gulp.task('clean', function(cb) {
	del('dist').then(function(cb) {
		return cache.clearAll(cb);
	})
});

gulp.task('clean:dist', function() {
	return del(['dist/**/*', '!dist/images', '!dist/images/**/*']);
});

gulp.task('images', function() {
	return gulp.src('app/images/**/*.+(png|jpg|gif|svg|)')
	.pipe(cache(imagemin({
		interlaced: true
	})))
	.pipe(gulp.dest('dist/images'))
});

gulp.task('fonts', function() {
	return gulp.src('app/fonts/**/*')
	.pipe(gulp.dest('dist/fonts'))
});

gulp.task('uglify', function() {
	var assets = uglify.assets();

	return gulp.src('app/*.html')
			.pipe(assets)
			.pipe(useref())
			.pipe(assets.uglify())
			.pipe(assets.restore)
			.pipe(gulp.dest('dist'))
});

gulp.task('useref', function() {
  var assets = useref.assets();

  return gulp.src('app/*.html')
    .pipe(assets)
    // Minifies only if it's a CSS file
    .pipe(gulpIf('*.css', minifyCSS()))
    // Uglifies only if it's a Javascript file
    .pipe(gulpIf('*.js', uglify()))
    .pipe(assets.restore())
    .pipe(useref())
    .pipe(gulp.dest('dist'))
});


// start watch tasks.
gulp.task('browserSync', function() {
	browserSync({
		server: {
			baseDir: 'app'
		}
	});
});

gulp.task('sass', function() {
	return gulp.src('app/scss/**/*.scss')
	.pipe(sass())
	.pipe(gulp.dest('app/css'))
	.pipe(browserSync.reload({
		stream: true
	}))

});

// on file change
gulp.task('watch', ['browserSync', 'sass'], function() {
	gulp.watch('app/scss/**/*.scss', ['sass']);
	gulp.watch('app/js/**/*.js', browserSync.reload);
	gulp.watch('app/**/*.html', browserSync.reload);
	gulp.watch('bower_components', browserSync.reload);
});

// when building production assests.
gulp.task('build', ['clean:dist', 'sass', 'useref', 'images', 'fonts'], function() {

});


// tasks to run on gulp init
gulp.task('default', function(cb) {
	runSequence(['sass', 'browserSync', 'watch']);
});